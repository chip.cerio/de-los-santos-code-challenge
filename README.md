# de-los-santos-code-challenge

Code Challenge for Richard De Los Santos

### Instagram clone app

Why we choose Instagram? Because why not? 

### Requirements

1. App should be written in Kotlin
1. must have these architectures: MVVM, Dagger2, Room, Repository layer
1. nice to haves: Unit Test, Coroutines

### Ask a question

When you have a question, you should create an issue and collaborators answers your question. If question is solved, we'll close the issue. If not, we'll try to answer and discuss it. 

### Submitting

When code challenge is done, send a Merge Request. 

### Code Review

Collaborators will reach out to you via Merge Request discussions. This is our way communicating people who work remotely. We understand that you have your day job, but quick responses are really appreciated. We also have our day job too :)

There will be 3 approvers for this code review challenge but there will be at least 2 minimum approves to make your Merge Request being merged to `master` branch

### Next Steps

When merge request is merged to `master`, expect a call from our HR for the next level :)
